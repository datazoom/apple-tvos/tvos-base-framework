Pod::Spec.new do |spec|
  spec.name         = "DZTvOSBase"
  spec.version      = "1.8.0"
  spec.summary      = "Datazoom tvos base collector"
  spec.description  = <<-DESC
          Datazoom tvOs base collector sdk used with other tvOS collectors, it collects the events from players and send to dz pipeline
	DESC

  spec.homepage     = "https://www.datazoom.io/"
  spec.license      = { :type => 'Custom', :text => <<-LICENSE
              		Datazoom, Inc ("COMPANY") CONFIDENTIAL
 Copyright (c) 2017-2018 [Datazoom, Inc.], All Rights Reserved.

 NOTICE:  All information contained herein is, and remains the property of COMPANY. The intellectual and technical concepts contained
 herein are proprietary to COMPANY and may be covered by U.S. and Foreign Patents, patents in process, and are protected by trade secret or copyright law.
 Dissemination of this information or reproduction of this material is strictly forbidden unless prior written permission is obtained
 from COMPANY.  Access to the source code contained herein is hereby forbidden to anyone except current COMPANY employees, managers or contractors who have executed 
 Confidentiality and Non-disclosure agreements explicitly covering such access.

 The copyright notice above does not evidence any actual or intended publication or disclosure  of  this source code, which includes  
 information that is confidential and/or proprietary, and is a trade secret, of  COMPANY.   ANY REPRODUCTION, MODIFICATION, DISTRIBUTION, PUBLIC  PERFORMANCE, 
 OR PUBLIC DISPLAY OF OR THROUGH USE  OF THIS  SOURCE CODE  WITHOUT  THE EXPRESS WRITTEN CONSENT OF COMPANY IS STRICTLY PROHIBITED, AND IN VIOLATION OF APPLICABLE 
 LAWS AND INTERNATIONAL TREATIES.  THE RECEIPT OR POSSESSION OF  THIS SOURCE CODE AND/OR RELATED INFORMATION DOES NOT CONVEY OR IMPLY ANY RIGHTS  
 TO REPRODUCE, DISCLOSE OR DISTRIBUTE ITS CONTENTS, OR TO MANUFACTURE, USE, OR SELL ANYTHING THAT IT  MAY DESCRIBE, IN WHOLE OR IN PART.
                  LICENSE
                }                
  spec.author             = { "vinu" => "vinu@xminds.com" }
  spec.source       = { :git => "https://gitlab.com/datazoom/apple-tvos/tvos-base-framework.git", :tag => "#{spec.version}" }
  spec.vendored_frameworks = "DzTvOSBase.framework"
  spec.platform = :tvos, "10.0"
  spec.swift_versions = "4.0"
end
